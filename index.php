<?php

use Composer\Semver\Comparator;
use GitWrapper\GitCommand;
use GitWrapper\GitWrapper;
use GitWrapper\Event\GitLoggerEventSubscriber;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Symfony\Component\Dotenv\Dotenv;

require 'vendor/autoload.php';

$since = 'v9.0.0';

$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env.dist');
$dotenv->loadEnv(__DIR__.'/.env');

$source = $_ENV['SOURCE_REPO'];
$target = $_ENV['TARGET_REPO'];

// Log to a file named "git.log"
$log = new Logger('git');
$log->pushHandler(new StreamHandler('git.log', Logger::DEBUG));

$gitSourceWrapper = new GitWrapper();
$gitSourceWrapper->addLoggerEventSubscriber(new GitLoggerEventSubscriber($log));

$sourceFolder = $_ENV['SOURCE_FOLDER'];
$targetFolder = $_ENV['TARGET_FOLDER'];

if (file_exists($sourceFolder)) {
    $gitSourceRepo = $gitSourceWrapper->workingCopy($sourceFolder);
    $gitSourceRepo->fetch();
} else {
    $gitSourceWrapper->setTimeout(600); // 10min
    $gitSourceRepo = $gitSourceWrapper->cloneRepository($source, $sourceFolder);
}

// TARGET
$gitTargetWrapper = new GitWrapper();
$gitTargetWrapper->addLoggerEventSubscriber(new GitLoggerEventSubscriber($log));

if (file_exists($targetFolder)) {
    $gitTargetRepo = $gitTargetWrapper->workingCopy($targetFolder);
    $gitTargetRepo->fetch();
} else {
    $gitTargetWrapper->setTimeout(600); // 10min
    $gitTargetRepo = $gitTargetWrapper->cloneRepository($target, $targetFolder);
}


// Clean changes
$gitSourceRepo->reset('--hard');
$gitSourceRepo->clean('-fd');
$gitTargetRepo->reset('--hard');
$gitSourceRepo->clean('-fd');

$listTagsCommand = new GitCommand('tag', '-l', '--sort=version:refname', 'v*');

$sourceTags = $gitSourceWrapper->run($listTagsCommand, $gitSourceRepo->getDirectory());
$sourceTags = explode(PHP_EOL, $sourceTags);

$targetTags = $gitTargetWrapper->run($listTagsCommand, $gitTargetRepo->getDirectory());
$targetTags = explode(PHP_EOL, $targetTags);

// Determine version changes
$diff = array_diff($sourceTags, $targetTags);

foreach ($diff as $version) {

    // Skip version if prior than $since
    if (Comparator::lessThan($version, $since)) {
        continue;
    }

    echo 'Detected ' . $version . PHP_EOL;

    // Checkout version tag
    $gitSourceRepo->checkout($version);

    // Rsync repositories
    $rsync = sprintf(
        "rsync -aL --delete --exclude=.git --exclude=/composer.json --exclude=/README.md '%s' '%s' 2>&1",
        $gitSourceRepo->getDirectory() . '/src/main/webapp/',
        $gitTargetRepo->getDirectory()
    );
    `$rsync`;

    // Add all changes (if file was to be ignored with .gitignore it should not be versioned in source repo)
    $command = new GitCommand('add', '.', '--force');
    $gitTargetWrapper->run($command, $gitTargetRepo->getDirectory());

    // Commit
    $gitTargetRepo->commit('Add jgraph/drawio ' . $version);
    $gitTargetRepo->tag($version);

    echo 'Added jgraph/drawio ' . $version . PHP_EOL;
}

$gitTargetRepo->push();
$gitTargetRepo->pushTags();
