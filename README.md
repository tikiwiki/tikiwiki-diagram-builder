# tikiwiki/diagram package builder

## Overview

This repository helps build and update tikiwiki/diagram package versions that mirrors jgraph/drawio repository release versions. See also: https://gitlab.com/tikiwiki/diagram

## Installation

### Install composer dependencies

```
composer install
```

### Setup .env

```
cp .env.dist .env
```

Edit the `SOURCE_FOLDER` and `TARGET_FOLDER` to the directories of your choice.

These directories can be existing git repositories, avoiding the need to checkout them again.

## Execution

```
php index.php
```

## Packagist

https://packagist.org/packages/tikiwiki/diagram
